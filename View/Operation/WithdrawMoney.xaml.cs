﻿using System.Windows.Controls;
using ViewModel.Operation;

namespace View.Operation
{
    /// <summary>
    /// Логика взаимодействия для Open_Score.xaml
    /// </summary>
    public partial class WithdrawMoney : UserControl
    {
        public WithdrawMoney()
        {
            InitializeComponent();
            DataContext = new WithdrawMoneyViewModel();
        }
    }
}

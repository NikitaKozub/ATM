﻿using Model;
using Model.Library;
using Model.newV;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ViewModel.PatternMVVM;

namespace ViewModel.Operation
{
    public class PutMoneyViewModel : Operation
    {
        RelayCommand putMoneyIntoAccount;
        /// <summary>
        /// Положить деньги на счет
        /// </summary>
        public RelayCommand PutMoneyIntoAccount
        {
            get
            {
                return putMoneyIntoAccount ??
                    (putMoneyIntoAccount = new RelayCommand((selectedItem) =>
                    {
                        if (putMoneyIntoAccount != null)
                        {
                            Message.ClearMessages();
                            int putMoney = Convert.ToInt32(OperationScoreModel.CurrentMoney.ToString());
                            Money money = new Money();
                            Atm.GetBillsFromATM();
                            Atm.GetMoneyFromATM();
                            if (money.TransformationOfMoneyIntoBills(PUT, Atm.Summ,
                            Atm.billFromATM, putMoney, Atm.billFromUser))
                            {
                                if (User.UpdateMoney(money.BillFromUser, putMoney))
                                {
                                    OperationScoreModel.Money = User.Score.Money.ToString();
                                    OperationScoreModel.CurrentMoney = 0;
                                    Message.AddMessage("Деньги отправлены");
                                    OperationScoreModel.Message = Message.GetMessages();
                                    StartWorkATM();
                                }
                                else
                                {
                                    OperationScoreModel.Message = Message.GetMessages();
                                }
                            }
                            else
                            {
                                Message.ClearMessages();
                                Message.AddMessage("Деньги положить не удалось");
                                OperationScoreModel.Message = Message.GetMessages();
                            }
                        }
                    }));
            }
        }
    }
}

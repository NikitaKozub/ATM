﻿using Model;
using Model.newV;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ViewModel.PatternMVVM;

namespace ViewModel.Operation
{
    public class Operation
    {
        public OperationScoreModel OperationScoreModel { get; set; }
        public User User { get; set; }
        /// <summary>
        /// Текущий счет
        /// </summary>
        public Score Current { get; set; }
        public List<Bill> BillFromATM { get; set; }
        public List<Bill> BillFromUser { get; set; }
        public ATM Atm;
        public const int MAX_SUMM = 10000;

        public const int PUT = 1;
        public Operation()
        {
            OperationScoreModel = new OperationScoreModel()
            {
                Score = "",
                Money = "",
                CurrentMoney = 0,
                OperationEnabled = false,
                EnabledLogin = true,
                IsEnabledBill = false
            };
            Atm = new ATM();
            Atm.GetBillsFromATM();
            BillFromATM = Atm.billFromATM;
            BillFromUser = Atm.billFromUser;
            Message.ClearMessages();
            Message.AddMessage("Войдите введя в номер вашего счета");
            
            OperationScoreModel.Message = Message.GetMessages();
        }

        /// <summary>
        /// Ввод купюр
        /// </summary>
        RelayCommand bill;
        public RelayCommand Bill
        {
            get
            {
                return bill ??
                    (bill = new RelayCommand((selectedItem) =>
                    {
                        if (bill != null)
                        {
                            if (Convert.ToInt32(selectedItem) == -1)
                            {
                                StartWorkATM();
                            }
                            else
                            {
                                Message.ClearMessages();
                                Atm.GetMoneyFromATM();
                                OperationScoreModel.CurrentMoney += Convert.ToInt32(selectedItem);
                                if (OperationScoreModel.CurrentMoney > MAX_SUMM || (Atm.Summ + OperationScoreModel.CurrentMoney) > 685500)
                                {
                                    if (OperationScoreModel.CurrentMoney > MAX_SUMM)
                                    {
                                        Message.AddMessage("Не больше "+ MAX_SUMM + " тыс можно положить за раз");
                                    }
                                    if ((Atm.Summ + OperationScoreModel.CurrentMoney) > 685500)
                                    {
                                        Message.AddMessage("Данная сумма не помещается в банкомат");
                                    }
                                    OperationScoreModel.Message = Message.GetMessages();
                                    OperationScoreModel.OperationEnabled = false;
                                    return;
                                }
                                OperationScoreModel.Message = Message.GetMessages();
                                OperationScoreModel.OperationEnabled = true;
                                OperationScoreModel.EnabledLogin = false;
                            }
                        }
                    }));
            }
        }

        /// <summary>
        /// Открытие своего счета
        /// </summary>
        RelayCommand login;
        public RelayCommand Login
        {
            get
            {
                return login ??
                    (login = new RelayCommand((selectedItem) =>
                    {
                        if (login != null)
                        {
                            Message.ClearMessages();
                            User = new User();
                            Message.ClearMessages();
                            Current = User.IsScore(Convert.ToInt32(OperationScoreModel.Score));
                            if (Current == null)
                            {
                                OperationScoreModel.OperationEnabled = false;
                                Message.AddMessage("Неправильно введен номер счета");
                                OperationScoreModel.Money = "0";
                                OperationScoreModel.Message = Message.GetMessages();
                                return;
                            }
                            if (Current != null)
                            {
                                User.Score = Current;
                                OperationScoreModel.OperationEnabled = false;
                                OperationScoreModel.IsEnabledBill = true;
                                OperationScoreModel.Money = Current.Money.ToString();
                                User.Score = Current;
                                Message.AddMessage("Вы вошли");
                                OperationScoreModel.Message = Message.GetMessages();
                            }
                        }
                    }));
            }
        }

        /// <summary>
        /// Отмена всего и начало работы с банкоматом
        /// </summary>
        protected void StartWorkATM()
        {
            Atm.GetBillsFromATM();
            BillFromATM = Atm.billFromATM;
            OperationScoreModel.CurrentMoney = 0;
            Message.ClearMessages();
        }
    }
}

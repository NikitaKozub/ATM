﻿using Model;
using Model.Library;
using Model.newV;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ViewModel.PatternMVVM;

namespace ViewModel.Operation
{
    public class WithdrawMoneyViewModel : Operation
    {
        private const int WITHDRAW = -1;
        Money money;
        RelayCommand withdrawMoneyFromAccountLarge;
        /// <summary>
        /// Транзакции для их отмены
        /// </summary>
        List<List<Bill>> transaction = new List<List<Bill>>();
        /// <summary>
        /// Снять деньги со счета
        /// </summary>
        public RelayCommand WithdrawMoneyFromAccountLarge
        {
            get
            {
                return withdrawMoneyFromAccountLarge ??
                    (withdrawMoneyFromAccountLarge = new RelayCommand((selectedItem) =>
                    {
                        if (withdrawMoneyFromAccountLarge != null)
                        {
                            if (Withdraw(OperationScoreModel.CurrentMoney))
                            {
                                OperationScoreModel.IsEnabledBill = false;
                                OperationScoreModel.EnabledLogin = true;
                                OperationScoreModel.OperationEnabled = false;
                            }
                            else
                            {
                                OperationScoreModel.CurrentMoney = 0;
                                Message.ClearMessages();
                                Message.AddMessage("Денег нет");
                                OperationScoreModel.Message = Message.GetMessages();
                                return;
                            }
                        }
                    }));
            }
        }

        RelayCommand withdrawMoneyFromAccountDespair;
        /// <summary>
        /// Снять деньги со счета (крупному)
        /// </summary>
        public RelayCommand WithdrawMoneyFromAccountDespair
        {
            get
            {
                return withdrawMoneyFromAccountDespair ??
                    (withdrawMoneyFromAccountDespair = new RelayCommand((selectedItem) =>
                    {
                        if (withdrawMoneyFromAccountDespair != null)
                        {
                            Message.ClearMessages();
                            if (Atm.Summ >= OperationScoreModel.CurrentMoney)
                            {
                                int value1 = OperationScoreModel.CurrentMoney / 2;
                                int value2 = OperationScoreModel.CurrentMoney / 2;
                                string value1String;
                                
                                if (OperationScoreModel.CurrentMoney <= MAX_SUMM &&
                                OperationScoreModel.CurrentMoney >= 5000)
                                {
                                    for (int i = 0; i < 2; i++)
                                    {
                                        value1 = OperationScoreModel.CurrentMoney / 4;
                                        value2 = OperationScoreModel.CurrentMoney / 4;
                                        value1String = value1.ToString();
                                        if (value1String.Length > 2 && value1String[value1String.Length - 1].ToString() != "0")
                                        {
                                            int tmp = Convert.ToInt32(value1String[value1String.Length - 1].ToString());
                                            value1 = value1 + tmp;
                                            value2 = value2 - tmp;
                                        }

                                        if (Withdraw(value1))
                                        {
                                            if (value2 == 0)
                                            {
                                                OperationScoreModel.CurrentMoney = 0;
                                                OperationScoreModel.Message = Message.GetMessages();
                                                return;
                                            }
                                            if (Withdraw(value2))
                                            {
                                                if (i == 1)
                                                {
                                                    OperationScoreModel.CurrentMoney = 0;
                                                    OperationScoreModel.Message = Message.GetMessages();
                                                    return;
                                                }
                                            }
                                            else
                                            {
                                                CancelTransaction();
                                            }
                                        }
                                        else
                                        {
                                            CancelTransaction();
                                        }
                                    }
                                }
                                if (OperationScoreModel.CurrentMoney < 5000 && OperationScoreModel.CurrentMoney > 500)
                                {
                                    value1 = OperationScoreModel.CurrentMoney / 2;
                                    value2 = OperationScoreModel.CurrentMoney / 2;
                                    value1String = value1.ToString();
                                    if (value1String.Length > 2 && value1String[value1String.Length - 1].ToString() != "0")
                                    {
                                        int tmp = Convert.ToInt32(value1String[value1String.Length - 1].ToString());
                                        value1 = value1 + tmp;
                                        value2 = value2 - tmp;
                                    }
                                    if (Withdraw(value1))
                                    {
                                        if (value2 == 0)
                                        {
                                            OperationScoreModel.CurrentMoney = 0;
                                            OperationScoreModel.Message = Message.GetMessages();
                                            return;
                                        }
                                        if (value2 != 0 && Withdraw(value2))
                                        {
                                            OperationScoreModel.CurrentMoney = 0;
                                            OperationScoreModel.Message = Message.GetMessages();
                                            return;
                                        }
                                        else
                                        {
                                            CancelTransaction();
                                        }
                                    }
                                    else
                                    {
                                        CancelTransaction();
                                    }
                                }
                                if (Withdraw(OperationScoreModel.CurrentMoney))
                                {
                                    OperationScoreModel.CurrentMoney = 0;
                                    OperationScoreModel.Message = Message.GetMessages();
                                    return;
                                }
                                else
                                {
                                    OperationScoreModel.CurrentMoney = 0;
                                    Message.AddMessage("Денег нет");
                                    OperationScoreModel.Message = Message.GetMessages();
                                    return;
                                }
                            }
                            else
                            {
                                OperationScoreModel.CurrentMoney = 0;
                                Message.AddMessage("Денег нет");
                                OperationScoreModel.Message = Message.GetMessages();
                                return;
                            }
                        }
                    }));
            }
        }


        public bool Withdraw(int summ)
        {
            Atm.GetBillsFromATM();
            Atm.GetMoneyFromATM();
            money = new Money();
            if (money.TransformationOfMoneyIntoBills(WITHDRAW, Atm.Summ,
                Atm.billFromATM, summ, Atm.billFromUser))
            {
                AddBillTransaction(money.BillFromUser);
                if (User.UpdateMoney(money.BillFromUser, WITHDRAW * summ))
                {
                    Message.AddMessage(money.BillFromUser);
                    OperationScoreModel.Message = Message.GetMessages();
                    return true;
                }
            }
            return false;
        }

        List<Bill> tmpBills = new List<Bill>();
        public void AddBillTransaction(List<Bill> bills)
        {
            if (transaction.Count > 0)
            {
                foreach (var itr in bills)
                {
                    Bill bill = new Bill(itr.Id, itr.Name, WITHDRAW * itr.Count);
                    tmpBills.Add(bill);
                }
                transaction.Add(tmpBills);
            }  
        }

        /// <summary>
        /// Отмена снятия денег
        /// </summary>
        public void CancelTransaction()
        {
            int transactionSumm = 0;
            if (transaction.Count > 0)
            {
                foreach (var itr in transaction)
                {
                    foreach (var it in itr)
                    {
                        transactionSumm += it.Name * ( WITHDRAW * it.Count);
                    }

                    User.UpdateMoney(itr, PUT * transactionSumm);
                }
            }
            transaction.Clear();
            tmpBills.Clear();
        }
    }
}

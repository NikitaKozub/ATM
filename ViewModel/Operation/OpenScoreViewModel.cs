﻿using Model;
using Model.Library;
using Model.newV;
using System;
using ViewModel.PatternMVVM;

namespace ViewModel.Operation
{
    public class OpenScoreViewModel
    {
        public OpenScoreModel OpenScoreModel { get; set; }
        public DataBase DataBase;
        public User User { get; set; }
        ATM Atm;
        private const int PUT = 1;
        public const int MAX_SUMM = 10000;
        public OpenScoreViewModel()
        {
            OpenScoreModel = new OpenScoreModel()
            {
                Score = "",
                Money = ""
            };
            Atm = new ATM();
            DataBase = new DataBase();
            Message.AddMessage("Введите номер счета и сумму для открытия");
            OpenScoreModel.Message = Message.GetMessages();
        }

        /// <summary>
        /// Открыть счет
        /// </summary>
        RelayCommand openScore;
        public RelayCommand OpenScore
        {
            get
            {
                return openScore ??
                    (openScore = new RelayCommand((selectedItem) =>
                    {
                        if (openScore != null)
                        {
                            Message.ClearMessages();
                            //Сумма при открытие счета
                            int summaForOpen = Convert.ToInt32(OpenScoreModel.Money);
                            if (summaForOpen >= 5000 & summaForOpen < MAX_SUMM & OpenScoreModel.Score != null)
                            {
                                Money money = new Money();
                                Atm.GetBillsFromATM();
                                Atm.GetMoneyFromATM();
                                money.TransformationOfMoneyIntoBills(PUT, Atm.Summ,
                                Atm.billFromATM, summaForOpen, Atm.billFromUser);
                                User = new User(Convert.ToInt32(OpenScoreModel.Score),
                                    summaForOpen, money.BillFromUser);
                                User.CreateUser();
                                OpenScoreModel.Message = Message.GetMessages();
                            }
                            else
                            {
                                if (OpenScoreModel.Score != null)
                                {
                                    Message.ClearMessages();
                                    Message.AddMessage("Счет введен не правильно");
                                    OpenScoreModel.Message = Message.GetMessages();
                                    return;
                                }
                                Message.ClearMessages();
                                Message.AddMessage("Сумма должна быть больше 5000 и меньше 100 000 тысяч");
                                OpenScoreModel.Message = Message.GetMessages();
                            }
                        }
                    }));
            }
        }
    }
}

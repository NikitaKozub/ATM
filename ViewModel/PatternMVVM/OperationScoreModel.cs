﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ViewModel.PatternMVVM
{
    public class OperationScoreModel : INotifyPropertyChanged
    {

        private string sorce;
        /// <summary>
        /// Счет
        /// </summary>
        public string Score
        {
            get { return sorce; }
            set
            {
                if (sorce != value)
                {
                    sorce = value;
                    OnPropertyChanged("Score");
                }
            }
        }

        private string money;
        /// <summary>
        /// Деньги на счету
        /// </summary>
        public string Money
        {
            get { return money; }
            set
            {
                if (money != value)
                {
                    money = value;
                    OnPropertyChanged("Money");
                }
            }
        }

        private int currentMoney;
        /// <summary>
        /// Выбранная сумма
        /// </summary>
        public int CurrentMoney
        {
            get { return currentMoney; }
            set
            {
                if (currentMoney != value)
                {
                    if (currentMoney > value)
                    {
                        currentMoney = 0;
                        OnPropertyChanged("CurrentMoney");
                        return;
                    }
                    currentMoney = value;
                    OnPropertyChanged("CurrentMoney");
                }
            }
        }

        
        private int bill;
        /// <summary>
        /// купюры
        /// </summary>
        public int Bill
        {
            get { return bill; }
            set
            {
                if (bill != value)
                {
                    bill = value;
                    OnPropertyChanged("Bill");
                }
            }
        }

        /// <summary>
        /// Можно ли нажимать на купюры
        /// </summary>
        private bool isEnabledBill;
        public bool IsEnabledBill
        {
            get { return isEnabledBill; }
            set
            {
                isEnabledBill = value;
                OnPropertyChanged("IsEnabledBill");
            }
        }

        /// <summary>
        /// Можно ли нажимать на действия с купюрами
        /// </summary>
        private bool operationEnabled;
        public bool OperationEnabled
        {
            get { return operationEnabled; }
            set
            {
                operationEnabled = value;
                OnPropertyChanged("OperationEnabled");
            }
        }

        
        /// <summary>
        /// Можно ли входить в систему
        /// </summary>
        private bool enabledLogin;
        public bool EnabledLogin
        {
            get { return enabledLogin; }
            set
            {
                enabledLogin = value;
                OnPropertyChanged("EnabledLogin");
            }
        }

        /// <summary>
        /// Сообщения
        /// </summary>
        private string _Message;
        public string Message
        {
            get { return _Message; }
            set
            {
                if (_Message != value)
                {
                    _Message = value;
                    OnPropertyChanged("Message");
                }
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}


﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ViewModel.PatternMVVM
{
    public class OpenScoreModel : INotifyPropertyChanged
    {
        private int num;
        private bool isNum;
        private StringBuilder tmp { set; get; }
        private string score;
        /// <summary>
        /// Счет
        /// </summary>
        public string Score
        {
            get { return score; }
            set
            {
                if (score != value)
                {
                    if (!(isNum = int.TryParse(value, out num)))
                    {
                        Model.Message.ClearMessages();
                        Model.Message.AddMessage("Вы должны ввести число");
                        OnPropertyChanged("Score");
                        return;
                    }
                    if (value.Length < 4 || value.Length > 4)
                    {
                        Model.Message.ClearMessages();
                        Model.Message.AddMessage("Вы должны ввести четырехзначное число");
                        OnPropertyChanged("Score");
                        return;
                    }
                    score = value;
                    OnPropertyChanged("Score");
                }
            }
        }

        private string money;
        /// <summary>
        /// Деньги для открытия
        /// </summary>
        public string Money
        {
            get { return money; }
            set
            {
                if (money != value)
                {
                    if (!(isNum = int.TryParse(value, out num)))
                    {
                        Model.Message.ClearMessages();
                        Model.Message.AddMessage("Вы должны ввести сумму из чисел");
                        money = "0";
                        OnPropertyChanged("Score");
                        return;
                    }
                    if (Convert.ToInt32(value) %10 != 0)
                    {
                        tmp = new StringBuilder(value);
                        tmp[3] = '0';
                        money = tmp.ToString();
                        Model.Message.AddMessage("Только купюры");
                        OnPropertyChanged("Score");
                        return;
                    }
                    if (num < 5000)
                    {
                        Model.Message.ClearMessages();
                        Model.Message.AddMessage("Вы должны ввести сумму больше или равную 5000");
                        money = "0";
                        OnPropertyChanged("Score");
                        return;
                    }
                    money = value;
                    OnPropertyChanged("Money");
                }
            }
        }

        /// <summary>
        /// Сообщения
        /// </summary>
        private string _Message;
        public string Message
        {
            get { return _Message; }
            set
            {
                if (_Message != value)
                {
                    _Message = value;
                    OnPropertyChanged("Message");
                }
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}

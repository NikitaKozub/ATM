﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Model;
using Model.Library;
using Model.newV;

namespace UnitTest
{
    [TestClass]
    public class UnitTestUser
    {
        ATM Atm;
        public UnitTestUser()
        {
            Atm = new ATM();
        }

        /// <summary>
        /// Проверка обновления купюр
        /// </summary>
        [TestMethod]
        public void TestMethodFileUpdateBill()
        {
            ATM atm = new ATM();
            List<Bill> updateBills = new List<Bill>();
            updateBills.Add(new Bill(1, 5000, 1));
            updateBills.Add(new Bill(1, 1000, -1));
            Assert.AreEqual(true, atm.UpdateBill(updateBills));
            
            atm.GetBillsFromATM();
            List<Bill> NewBills;
            NewBills = atm.billFromATM;
            int i = -1;
            foreach (var itr in NewBills)
            {
                i++;
                if (itr.Name == 5000)
                {
                    Assert.AreEqual(3, itr.Count);
                    continue;
                }
                if (itr.Name == 1000)
                {
                    Assert.AreEqual(4, itr.Count);
                    continue;
                }
                Assert.AreEqual(atm.billFromATM[i].Count, itr.Count);
            }
            ////////////////////////////////////////////////////
            updateBills = new List<Bill>();
            updateBills.Add(new Bill(1, 5000, -1));
            updateBills.Add(new Bill(1, 1000, 1));
            atm.UpdateBill(updateBills);
            NewBills = atm.billFromATM;
            i = -1;
            foreach (var itr in NewBills)
            {
                i++;
                Assert.AreEqual(atm.billFromATM[i].Count, itr.Count);
            }
        }

        [TestMethod]
        public void TestMethodTransformationBill()
        {
            Atm.GetBillsFromATM();
            Atm.GetMoneyFromATM();

            Money money = new Money();
            int summ = 1470;
            money.TransformationOfMoneyIntoBills(-1, Atm.Summ,
                Atm.billFromATM, summ, Atm.billFromUser);

            foreach (var itr in money.BillFromUser)
            {
                summ += itr.Name * itr.Count;
            }
            Assert.AreEqual(0, summ);

            summ = 5600;
            money.TransformationOfMoneyIntoBills(-1, Atm.Summ,
                Atm.billFromATM, summ, Atm.billFromUser);

            foreach (var itr in money.BillFromUser)
            {
                summ += itr.Name * itr.Count;
            }
            Assert.AreEqual(0, summ);
        }
    }
}

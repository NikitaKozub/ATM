﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.newV
{
    /// <summary>
    /// Запись пользователя
    /// </summary>
    public class Score
    {
        /// <summary>
        /// Id счета
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// Счет пользователя
        /// </summary>
        public int _Score { get; set; }
        /// <summary>
        /// Деньги пользователя
        /// </summary>
        public int Money { get; set; }

        /// <summary>
        /// Положить деньги в банкомат
        /// </summary>
        /// <param name="money"></param>
        public void AcceptMoney(int money)
        {
            Money += money;
        }

        /// <summary>
        /// Взять деньги со счета
        /// </summary>
        /// <param name="money">нужная сумма</param>
        public bool GiveOutMoney(int money)
        {
            if (Money + money >= 0)
            {
                Money += money;
                return true;
            }
            Message.AddMessage("Вы не располагаете такой суммой");
            return false;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.newV
{
    /// <summary>
    /// Класс купюра
    /// </summary>
    public class Bill
    {
        public int Name;
        public int Count;
        public int Id;
        public Bill(int id, int name, int count)
        {
            Id = id;
            Name = name;
            Count = count;
        }
    }

}

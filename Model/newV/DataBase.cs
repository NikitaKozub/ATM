﻿using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.newV
{
    public class DataBase
    {
        public FileInfo existingFile;
        public ExcelPackage package;
        public ExcelWorksheet worksheet;
        private const int LIST_EXCEL = 1;
        private const int COLUMN_SCORE = 1;
        private const int COLUMN_MONEY = 2;
        private const int COLUMN_MONEY_ATM = 3;
        private const int COLUMN_NAME_BILL_ATM = 5;
        private const int COLUMN_NUMBER_BILL_ATM = 6;
        private string Path;
        List<Bill> Bills { get; set; }
        public DataBase(List<Bill> bills)
        {
            Path = "./Users.xlsx";
            Bills = bills;
            CheckFileUsers();
        }
        public DataBase()
        {
            Path = "./Users.xlsx";
            CheckFileUsers();
        }

        private void CheckFileUsers()
        {
            if (File.Exists(Path) == false)
            {
                Create();
            }
        }

        /// <summary>
        /// Создание базы данных, если ее нет с начальными значениями
        /// </summary>
        public void Create()
        {
            using (ExcelPackage excelPackage = new ExcelPackage())
            {
                excelPackage.Workbook.Properties.Author = "Nikita";
                excelPackage.Workbook.Properties.Title = "UsersATM";
                excelPackage.Workbook.Properties.Created = DateTime.Now;

                ExcelWorksheet worksheet = excelPackage.Workbook.Worksheets.Add("ScoreAndMoney");
                // Создание Первого тестового пользователя
                worksheet.Cells[1, COLUMN_SCORE].Value = 1234;
                worksheet.Cells[1, COLUMN_MONEY].Value = 0;

                // Заполнение купюрами
                int summ = 0;
                foreach (var itr in Bills)
                {
                    worksheet.Cells[itr.Id, COLUMN_NAME_BILL_ATM].Value = itr.Name;
                    worksheet.Cells[itr.Id, COLUMN_NUMBER_BILL_ATM].Value = itr.Count;
                    summ += itr.Name * itr.Count;
                    worksheet.Cells[1, COLUMN_MONEY_ATM].Value = summ;
                }

                FileInfo fi = new FileInfo(Path);
                excelPackage.SaveAs(fi);
            }
        }

        /// <summary>
        /// Поиск записи в файле
        /// </summary>
        /// <param name="score">номер счета(запись)</param>
        /// <returns>возращает объект, либо null</returns>
        public Score IsScoreFromFile(int score)
        {
            Score Score;
            FileInfo fi = new FileInfo(Path);
            using (ExcelPackage excelPackage = new ExcelPackage(fi))
            {
                worksheet = excelPackage.Workbook.Worksheets[LIST_EXCEL];
                int i = 1;
                while (worksheet.Cells[i, COLUMN_SCORE].Value != null)
                {
                    if (Convert.ToInt32(worksheet.Cells[i, COLUMN_SCORE].Value) == score)
                    {
                        Score = new Score();
                        Score.Id = i;
                        Score._Score = Convert.ToInt32(worksheet.Cells[i, COLUMN_SCORE].Value);
                        Score.Money = Convert.ToInt32(worksheet.Cells[i, COLUMN_MONEY].Value);
                        return Score;
                    }
                    i++;
                }
            }
            return null;
        }

        /// <summary>
        /// Возвращает все купюры из банкомате
        /// </summary>
        /// <returns></returns>
        public List<Bill> GetBillFromDataBase()
        {
            var bills = new List<Bill>();
            FileInfo fi = new FileInfo(Path);
            using (ExcelPackage excelPackage = new ExcelPackage(fi))
            {
                worksheet = excelPackage.Workbook.Worksheets[LIST_EXCEL];
                int row = 1;
                while (worksheet.Cells[row, COLUMN_NAME_BILL_ATM].Value != null)
                {
                    bills.Add(new Bill(row, 
                       Convert.ToInt32(worksheet.Cells[row, COLUMN_NAME_BILL_ATM].Value),
                       Convert.ToInt32(worksheet.Cells[row, COLUMN_NUMBER_BILL_ATM].Value)));
                    row++;
                }
            }
            return bills;
        }

        /// <summary>
        /// Общая сумма денег в банкомате
        /// </summary>
        /// <returns></returns>
        public int GetMoneyFromBill()
        {
            int money = 0;
            FileInfo fi = new FileInfo(Path);
            using (ExcelPackage excelPackage = new ExcelPackage(fi))
            {
                worksheet = excelPackage.Workbook.Worksheets[LIST_EXCEL];
                int row = 1;
                if (worksheet.Cells[row, COLUMN_MONEY_ATM].Value != null)
                {
                    money = Convert.ToInt32(worksheet.Cells[row, COLUMN_MONEY_ATM].Value);
                }
            }
            return money;
        }

        /// <summary>
        /// Создание нового счета в базе данных
        /// </summary>
        /// <param name="score">счет</param>
        /// <param name="money">сумма при открытие</param>
        public void WriteFileScore(int score, int money)
        {
            FileInfo fi = new FileInfo(Path);
            using (ExcelPackage excelPackage = new ExcelPackage(fi))
            {
                worksheet = excelPackage.Workbook.Worksheets[LIST_EXCEL];
                int row = 1;
                while (worksheet.Cells[row, COLUMN_SCORE].Value != null)
                {
                    row++;
                }
                worksheet.Cells[row, COLUMN_SCORE].Value = score;
                worksheet.Cells[row, COLUMN_MONEY].Value = money;
                worksheet.Cells[1, COLUMN_MONEY_ATM].Value = Convert.ToInt32(worksheet.Cells[1, COLUMN_MONEY_ATM].Value) + money;
                excelPackage.Save();
            }
        }

        /// <summary>
        /// Обновление купюр в банкомате
        /// </summary>
        /// <param name="bill">новое состояние купюр (>0 стало больше, <0 стало меньше)</param>
        /// <returns>удачно или нет все обновлено</returns>
        public bool UpdataeDataBaseBillsFromAtm(List<Bill> bills)
        {
            FileInfo fi = new FileInfo(Path);
            using (ExcelPackage excelPackage = new ExcelPackage(fi))
            {
                worksheet = excelPackage.Workbook.Worksheets[LIST_EXCEL];
                int row = 1;
                int columnNameBillAtm;
                int countBill;
                while (worksheet.Cells[row, COLUMN_NAME_BILL_ATM].Value != null)
                {
                    columnNameBillAtm = Convert.ToInt32(worksheet.Cells[row, COLUMN_NAME_BILL_ATM].Value);
                    foreach (var itr in bills)
                    {
                        if (columnNameBillAtm == itr.Name)
                        {
                            countBill = Convert.ToInt32(worksheet.Cells[row, COLUMN_NUMBER_BILL_ATM].Value) + itr.Count;
                            if (countBill > 100)
                            {
                                Message.AddMessage("К сожалению ваши деньги не поместятся в банкомат");
                                return false;
                            }
                            if (countBill < 0)
                            {
                                Message.AddMessage("К сожалению в банкомате не хватает купюр");
                                return false;
                            }
                            worksheet.Cells[row, COLUMN_NUMBER_BILL_ATM].Value = countBill;
                        }
                    }
                    row++;
                }
                excelPackage.Save();
            }
            return true;
        }

        /// <summary>
        /// Обновление пользовательских данных
        /// </summary>
        /// <param name="score">счет</param>
        /// <param name="putMoney">сколько положили(+), сколько сняли(-)</param>
        public void UpdateUserDate(Score score, int putMoney)
        {
            FileInfo fi = new FileInfo(Path);
            using (ExcelPackage excelPackage = new ExcelPackage(fi))
            {
                worksheet = excelPackage.Workbook.Worksheets[LIST_EXCEL];
                if (worksheet.Cells[score.Id, COLUMN_SCORE].Value != null)
                {
                    worksheet.Cells[score.Id, COLUMN_SCORE].Value = score._Score;
                    worksheet.Cells[score.Id, COLUMN_MONEY].Value = score.Money;
                    worksheet.Cells[1, COLUMN_MONEY_ATM].Value = Convert.ToInt32(worksheet.Cells[1, COLUMN_MONEY_ATM].Value) + putMoney;
                }
                excelPackage.Save();
            }
        }
    }
}

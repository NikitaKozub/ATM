﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    /// <summary>
    /// Сообщения для пользователя
    /// </summary>
    public static class Message
    {
        /// <summary>
        /// Все сообщения
        /// </summary>
        public static List<String> Messages = new List<string>();

        /// <summary>
        /// Добавить сообщения
        /// </summary>
        /// <param name="message">сообщение</param>
        public static void AddMessage(string message)
        {
            Messages.Add(message);
        }

        public static void AddMessage(List<newV.Bill> bills)
        {
            foreach (var itr in bills)
            {
                if(itr.Count < 0 | itr.Count > 0)
                {
                    Messages.Add("Банкноты: " + itr.Name + " " + itr.Count +" ");
                }
            }
        }

        /// <summary>
        /// Очистить сообщения
        /// </summary>
        public static void ClearMessages()
        {
            Messages.Clear();
        }

        public static string GetMessages()
        {
            return String.Join(", ", Message.Messages.ToArray());
        }
    }
}

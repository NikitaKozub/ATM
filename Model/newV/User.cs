﻿using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.newV
{
    /// <summary>
    /// Пользователи бакомата
    /// </summary>
    public class User
    {
        //Счет
        public Score Score;
        public List<Bill> Bills;
        public ATM Atm;
        public User()
        {
            Atm = new ATM();
        }

        public User(int numberScore, int money, List<Bill> bill)
        {
            Bills = bill;
            Score = new Score();
            Score._Score = numberScore;
            Score.Money = money;
            Atm = new ATM();
        }

        public void CreateUser()
        {
            if (Score.Money == 0 || Score._Score == 0)
            {
                Message.AddMessage("Введите счет из 4 чисел и сумму больше или равную 5000");
                return;
            }
            //Bills
            Score score = IsScore(Score._Score);
            if (score == null)
            {
                if (Atm.UpdateBill(Bills))
                {
                    Atm.CreateScore(Score);
                    Message.AddMessage("Счет создан");
                    return;
                }
                else
                {
                    return;
                }
            }
            Message.AddMessage("Такой счет уже создан, выберете другой номер");
            return;
        }

        /// <summary>
        /// Поиск счета
        /// </summary>
        /// <param name="score">счет</param>
        /// <returns>возращает объект, либо null</returns>
        public Score IsScore(int score)
        {
            return Atm.FindScore(score);
        }

        /// <summary>
        /// Обновить счет пользователя
        /// </summary>
        /// <param name="bill">Купюры</param>
        /// <param name="putMoney"> 0 < - снять, 0> - положить</param>
        /// <returns></returns>
        public bool UpdateMoney(List<Bill> bill, int putMoney)
        {
            if (Score.Money + putMoney < 0)
            {
                Message.AddMessage("Денег не хватает");
                return false;
            }
            if (Score.Money + putMoney >= 0)
            {
                if (Atm.UpdateBill(bill))
                {
                    if(putMoney > 0)
                    {
                        Score.AcceptMoney(putMoney);
                    }
                    else
                    {
                        Score.GiveOutMoney(putMoney);
                    }
                    Atm.UpdateUser(Score, putMoney);
                    return true;
                }
                else
                {
                    return false;
                }
            }
            return false;
        }
    }
}

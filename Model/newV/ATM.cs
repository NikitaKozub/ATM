﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.newV
{
    public class ATM
    {
        public List<Bill> billFromATM;
        public List<Bill> billFromUser;
        public DataBase DataBase;
        /// <summary>
        /// Общая сумма в банкомате из всех купюр
        /// </summary>
        public int Summ;
        public ATM()
        {
            Summ = -1;
            billFromATM = new List<Bill>();
            billFromUser = new List<Bill>();
            StartWorkDataBase();
            DataBase = new DataBase(billFromATM);
        }
        private void StartWorkDataBase()
        {
            billFromATM.Add(new Bill(1, 5000, 2));
            billFromATM.Add(new Bill(2, 1000, 5));
            billFromATM.Add(new Bill(3, 500, 4));
            billFromATM.Add(new Bill(4, 200, 5));
            billFromATM.Add(new Bill(5, 100, 10));
            billFromATM.Add(new Bill(6, 50, 10));
            billFromATM.Add(new Bill(7, 10, 50));

            billFromUser.Add(new Bill(1, 5000, 0));
            billFromUser.Add(new Bill(2, 1000, 0));
            billFromUser.Add(new Bill(3, 500, 0));
            billFromUser.Add(new Bill(4, 200, 0));
            billFromUser.Add(new Bill(5, 100, 0));
            billFromUser.Add(new Bill(6, 50, 0));
            billFromUser.Add(new Bill(7, 10, 0));
        }

        /// <summary>
        /// Возвращает все купюры в банкомате
        /// </summary>
        public void GetBillsFromATM()
        {
            billFromATM = DataBase.GetBillFromDataBase();
        }

        /// <summary>
        /// Обновляет сумму денег в банкомате
        /// </summary>
        public void GetMoneyFromATM()
        {
            Summ = DataBase.GetMoneyFromBill();
        }

        /// <summary>
        /// Обновляет все купюры в банкомате
        /// </summary>
        /// <param name="bills">Купюры((+) - положить, (-) - снять)</param>
        /// <returns></returns>
        public bool UpdateBill(List<Bill> bills)
        {
            return DataBase.UpdataeDataBaseBillsFromAtm(bills);
        }

        public void CreateScore(Score score)
        {
            DataBase.WriteFileScore(score._Score, score.Money);
        }

        /// <summary>
        /// Обновления пользовательских данных
        /// </summary>
        /// <param name="score">счет пользователя</param>
        /// <param name="money">деньги(+ положить, - взять)</param>
        public void UpdateUser(Score score, int money)
        {
            DataBase.UpdateUserDate(score, money);
        }

        /// <summary>
        /// Поиск счет возвращает если есть или null
        /// </summary>
        /// <param name="score">номер счета</param>
        /// <returns></returns>
        public Score FindScore(int score)
        {
            return DataBase.IsScoreFromFile(score);
        }
    }
}

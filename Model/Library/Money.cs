﻿using Model.newV;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.Library
{
    public class Money
    {
        List<Bill> BillFromATM;
        int SummaFromATM;
        public List<Bill> BillFromUser;
        int SummaCurrent;
        List<Bill> TmpBill;

        //DataBase DataBase;
        //public Money()
        //{
        //    DataBase = new DataBase();
        //}

        /// <summary>
        /// Деньги в купюры 
        /// </summary>
        /// <param name="state">состояние(1 - положить, (-1) - взять)</param>
        public bool TransformationOfMoneyIntoBills(int state, 
            int summaFromATM, List<Bill> billFromATM,
            int summaCurrent, List<Bill> billFromUser)
        {
            SummaFromATM = summaFromATM;
            BillFromATM = billFromATM;
            SummaCurrent = summaCurrent;
            BillFromUser = billFromUser;
            TmpBill = new List<Bill>();


            if (SummaFromATM < SummaCurrent && state < 0)
            {
                Message.AddMessage("В банкомате не хватает денег");
                return false;
            }
            if (state == -1)
            {
                foreach (var itr in BillFromUser)
                {
                    if (SummaCurrent >= itr.Name)
                    {
                        SummaCurrent += CountBills(state, SummaCurrent, itr);
                    }
                }
            }
            else
            {
                foreach (var itr in BillFromATM)
                {
                    if (SummaCurrent >= itr.Name)
                    {
                        SummaCurrent -= CountBills(state, SummaCurrent, itr);
                    }
                }
            }
            
            
            if (SummaCurrent != 0)
            {
                return false;
            }
            BillFromUser = TmpBill;
            return true;
        }


        /// <summary>
        /// Подсчитывает количество купюр
        /// </summary>
        /// <param name="state"></param>
        /// <returns></returns>
        public int CountBills(int state, int summ, Bill bill)
        {
            if (state == -1)
            {
                foreach (var itr in BillFromATM)
                {
                    if (itr.Name == bill.Name)
                    {
                        if (summ / bill.Name <= itr.Count)
                        {
                            TmpBill.Add(new Bill(bill.Id, bill.Name, state * summ / bill.Name));
                            int count = summ / bill.Name;
                            return state * bill.Name * count;
                        }
                        else
                        {
                            TmpBill.Add(new Bill(bill.Id, bill.Name, state * itr.Count));
                            return state * bill.Name * itr.Count;
                        }
                    }
                }
            }
            if(state == 1)
            {
                foreach (var itr in BillFromATM)
                {
                    if (itr.Name == bill.Name)
                    {
                        TmpBill.Add(new Bill(bill.Id, bill.Name, state * summ / bill.Name));
                        int count = summ / bill.Name;
                        return state * bill.Name * count;
                    }
                }
            }
            return 1;
        }
    }
}
